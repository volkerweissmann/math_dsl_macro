For example
```rust
# #[macro_use]
# extern crate math_dsl_macro;
# fn main() {
# let psi = 1f64;
tex_expr!(r"\frac\pi 3 (1-\cos^2\psi)^2");
# }
```
Gets expanded zu something close to
```rust
# let psi = 1f64;
std::f64::consts::PI / 3f64  * ( 1f64 - psi.cos().powi(2) ).powi(2);
```
Similarly,
```rust
# #[macro_use]
# extern crate math_dsl_macro;
# fn main() {
# let psi = 1f64;
tex_equation!(r"x=\frac\pi 3 (1-\cos^2\psi)^2");
# }
```
gets expanded to
```rust
# let psi = 1f64;
let x = std::f64::consts::PI / 3f64  * ( 1f64 - psi.cos().powi(2) ).powi(2);
```
If, for example, `myinput.tex` contains
```latex
Other \LaTeX code
\begin{align*}
x=\frac\pi 3 (1-\cos^2\psi)^2
\end{align*}
Other \LaTeX code
\begin{align*}
a=b+c
\end{align*}
Other \LaTeX code
```
then
```rust ignore
from_latex_file!("myinput.tex");
```
gets expanded to
```rust
# let psi = 1f64;
# let b = 1f64;
# let c = 1f64;
let x = std::f64::consts::PI / 3f64  * ( 1f64 - psi.cos().powi(2) ).powi(2);
let a = b + c;
```
