#![doc = include_str!("../README.md")]

#[cfg(test)]
extern crate assert_approx_eq;
extern crate enum_as_inner;
extern crate pest;
extern crate pest_consume;
extern crate quote;
use proc_macro::TokenStream;
use quote::ToTokens;
use syn::{parse_macro_input, parse_quote, LitStr, Stmt};

mod ast;
mod codegen;
mod pest_parse;

// BEGIN helper functions for procedual macros
trait MyTrait {
    fn to_proc_ret(self) -> TokenStream;
}
impl<T: ToTokens> MyTrait for T {
    fn to_proc_ret(self) -> TokenStream {
        proc_macro::TokenStream::from(self.into_token_stream())
    }
}
// END

#[proc_macro]
pub fn tex_expr(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as LitStr).value();
    let term = pest_parse::parse_term(&input);
    term.ast_to_syn().to_proc_ret()
}

#[proc_macro]
pub fn tex_equation(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as LitStr).value();
    let eqn = pest_parse::parse_equation(&input);
    eqn.ast_to_syn().to_proc_ret()
}

#[proc_macro]
pub fn from_latex_file(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as LitStr).value();
    let str = std::fs::read_to_string(std::path::Path::new(&input)).unwrap();
    let re = regex::Regex::new("(?s)\\\\begin\\{align\\*\\}(.*?)\\\\end\\{align\\*\\}").unwrap();
    let mut eqs = Vec::new();
    for eqn in re.captures_iter(&str) {
        eqs.append(&mut pest_parse::parse_old_align(&eqn[0]));
    }
    let eqs: Vec<Stmt> = eqs.iter().map(|eqn| eqn.ast_to_syn()).collect();
    // Why doesn't syn define a type that is the result of such a parse_quote
    // and implements ToTokens?
    let stmts: Vec<Stmt> = parse_quote! {
        #(#eqs)*
    };
    stmts.iter().map(|s| s.to_proc_ret()).collect()
}

#[cfg(test)]
#[allow(non_snake_case)]
mod tests {
    use super::*;

    #[test]
    fn expand_proc_macro() {
        let input = r"\frac\pi 3 (1-\cos^2\psi)^2";
        let term = pest_parse::parse_term(input);
        let x = term.ast_to_syn().into_token_stream();
        println!("{}", x);
    }
}
