use crate::ast::{Assignment, CmpOp, Condition, Symbol, Term};
use pest_consume::{match_nodes, Error, Parser};

type Node<'i> = pest_consume::Node<'i, Rule, ()>;

#[derive(Parser)]
#[grammar = "latex.pest"]
struct LatexParser;

#[pest_consume::parser]
impl LatexParser {
    fn EOI(_input: Node) -> Result<(), Error<Rule>> {
        Ok(())
    }
    fn number(input: Node) -> Result<Term, Error<Rule>> {
        let x = input.as_str().parse::<f64>().map_err(|e| input.error(e))?;
        Ok(Term::Number(x))
    }
    fn single_digit_number(input: Node) -> Result<Term, Error<Rule>> {
        let x = input
            .as_str()
            .trim()
            .parse::<f64>()
            .map_err(|e| input.error(e))?;
        Ok(Term::Number(x))
    }
    fn letter(input: Node) -> Result<Term, Error<Rule>> {
        Ok(Term::Symbol(Symbol::str(input.as_str())))
    }
    fn underscore_arg(input: Node) -> Result<&str, Error<Rule>> {
        Ok(input.as_str())
    }
    fn braket(input: Node) -> Result<Term, Error<Rule>> {
        let childs = input.into_children();
        Ok(match_nodes!(childs;
            [opening_braket(_), sum(a), closing_braket(_)] => a,
        ))
    }
    fn symbol(input: Node) -> Result<Term, Error<Rule>> {
        Ok(Term::Symbol(Symbol::str(input.as_str())))
    }
    fn single_arg_func(input: Node) -> Result<&str, Error<Rule>> {
        Ok(input.as_str())
    }
    fn function(input: Node) -> Result<Term, Error<Rule>> {
        let childs = input.into_children();
        Ok(match_nodes!(childs;
            [single_arg_func(a), basic(b)] => {
                if a == "\\exp" {
                    Term::Pow(Box::new((Term::EulersNumber, b)))
                } else if a == "\\sin" {
                    Term::Sin(Box::new(b))
                } else if a == "\\cos" {
                    Term::Cos(Box::new(b))
                } else {
                    panic!();
                }
            },
            [single_arg_func(a), exp_arg(c), basic(b)] => {
                if a == "\\sin" {
                    Term::Pow(Box::new((Term::Sin(Box::new(b)), c)))
                } else if a == "\\cos" {
                    Term::Pow(Box::new((Term::Cos(Box::new(b)), c)))
                } else {
                    panic!();
                }
            },
        ))
    }
    fn basic(input: Node) -> Result<Term, Error<Rule>> {
        let childs = input.into_children();
        Ok(match_nodes!(childs;
            [letter(a)] => a,
            [letter(a), exp_arg(b)] => Term::Pow(Box::new((a, b))),
            [letter(a), underscore_arg(c)] => Term::Symbol(Symbol::str(&(a.into_symbol().unwrap().0 + c))),
            [letter(a), exp_arg(b), underscore_arg(c)] => Term::Pow(Box::new((Term::Symbol(Symbol::str(&(a.into_symbol().unwrap().0 + c))), b))),
            [letter(a), underscore_arg(c), exp_arg(b)] => Term::Pow(Box::new((Term::Symbol(Symbol::str(&(a.into_symbol().unwrap().0 + c))), b))),
            [braket(a)] => a,
            [braket(a), exp_arg(b)] => Term::Pow(Box::new((a, b))),
            [number(a)] => a,
            [number(a), exp_arg(b)] => Term::Pow(Box::new((a, b))),
            [fraction(a)] => a,
            [root(a)] => a,
            [function(a)] => a,
        ))
    }
    fn exp_arg(input: Node) -> Result<Term, Error<Rule>> {
        let childs = input.into_children();
        Ok(match_nodes!(childs;
            [valid_tex_arg(a)] => a,
        ))
    }
    fn div(input: Node) -> Result<Term, Error<Rule>> {
        Ok(match_nodes!(input.into_children();
            [basic(a).., basic(b)] => {
                let mut vec: Vec<Term> = a.collect();
                vec.push(Term::Pow(Box::new((b, Term::Number(-1.)))));
                Term::Mul(vec)
            },
        ))
    }
    fn frac_cmd(input: Node) -> Result<(), Error<Rule>> {
        Ok(())
    }
    fn root_cmd(input: Node) -> Result<(), Error<Rule>> {
        Ok(())
    }
    fn cdot(input: Node) -> Result<(), Error<Rule>> {
        Ok(())
    }
    fn opening_braket(input: Node) -> Result<(), Error<Rule>> {
        Ok(())
    }
    fn closing_braket(input: Node) -> Result<(), Error<Rule>> {
        Ok(())
    }
    fn valid_tex_arg(input: Node) -> Result<Term, Error<Rule>> {
        Ok(match_nodes!(input.into_children();
            [letter(a)] => a,
            [single_digit_number(a)] => a,
            [sum(a)] => a,
        ))
    }
    fn fraction(input: Node) -> Result<Term, Error<Rule>> {
        Ok(match_nodes!(input.into_children();
            [frac_cmd(_), valid_tex_arg(a), valid_tex_arg(b)] => {
                let vec = vec![a, Term::Pow(Box::new((b, Term::Number(-1.))))];
                Term::Mul(vec)
            },
        ))
    }
    fn root(input: Node) -> Result<Term, Error<Rule>> {
        Ok(match_nodes!(input.into_children();
            [root_cmd(_), valid_tex_arg(b)] => {
                Term::Pow(Box::new((b, Term::Number(1./2.))))
            },
            [root_cmd(_), sum(a), valid_tex_arg(b)] => {
                Term::Pow(Box::new((b, a)))
            },
        ))
    }
    fn mul(input: Node) -> Result<Term, Error<Rule>> {
        Ok(match_nodes!(input.into_children();
            [basic(a)] => a,
            [basic(a)..] => Term::Mul(a.collect()),
            [basic(a), cdot(_), mul(b)] => {
                let mut vec = vec![a];
                vec.append(&mut b.into_mul().unwrap());
                Term::Mul(vec)
            },
            [basic(a).., cdot(_), mul(b)] => {
                let mut vec = a.collect::<Vec<Term>>();
                vec.append(&mut b.into_mul().unwrap());
                Term::Mul(vec)
            },
            [div(a)] => a,
            [sum(a)] => a,
        ))
    }
    // If you want to know, why I do not like pest_consume, read this function.
    // It looks horrible and it took me many long, because the documentation
    // about the types is not perfect.
    fn sum(input: Node) -> Result<Term, Error<Rule>> {
        let mut childs = input.into_children().into_iter().peekable();
        let mut first = true;
        let mut summands = Vec::new();
        while childs.peek().is_some() {
            let negsign = if first {
                first = false;
                let el = childs.peek().unwrap();
                match el.as_rule() {
                    Rule::sign => match el.as_str() {
                        "+" => {
                            childs.next();
                            false
                        }
                        "-" => {
                            childs.next();
                            true
                        }
                        _ => panic!(),
                    },
                    _ => false,
                }
            } else {
                let sgn = childs.next().unwrap();
                if let Rule::sign = sgn.as_rule() {
                    match sgn.as_str() {
                        "+" => false,
                        "-" => true,
                        _ => panic!(),
                    }
                } else {
                    panic!()
                }
            };
            let mul = childs.next().unwrap();
            let mul = if let Rule::mul = mul.as_rule() {
                LatexParser::mul(mul)?
            } else {
                panic!();
            };
            if negsign {
                summands.push(Term::Mul(vec![mul, Term::Number(-1.)]));
            } else {
                summands.push(mul);
            }
        }
        if summands.len() == 1 {
            Ok(summands[0].clone())
        } else {
            Ok(Term::Sum(summands))
        }
    }
    fn cmp_op(input: Node) -> Result<CmpOp, Error<Rule>> {
        let op = match input.as_str().trim() {
            "=" => Ok(CmpOp::Equal),
            "\\stackrel ! =" => Ok(CmpOp::NotEqual),
            "<" => Ok(CmpOp::Lesser),
            ">" => Ok(CmpOp::Greater),
            "\\leq" => Ok(CmpOp::LesserEqual),
            "\\geq" => Ok(CmpOp::GreaterEqual),
            other => Err(format!("unknown comparison operator: '{}' ", other)),
        };
        op.map_err(|e| input.error(e))
    }
    fn case(input: Node) -> Result<(Term, Condition), Error<Rule>> {
        Ok(match_nodes!(input.into_children();
            [sum(a), sum(b), cmp_op(c), sum(d)] => (a, Condition{lhs: b,cmp_op: c,rhs: d}),
        ))
    }
    fn term_outer(input: Node) -> Result<Term, Error<Rule>> {
        Ok(match_nodes!(input.into_children();
            [sum(a)] => a,
            [case(a)..] => {
                let vec: Vec<(Term, Condition)> = a.collect();
                Term::SectionWise(vec)
            },
        ))
    }
    fn term(input: Node) -> Result<Term, Error<Rule>> {
        Ok(match_nodes!(input.into_children();
            [term_outer(a), EOI(_)] => a,
        ))
    }
    fn equation(input: Node) -> Result<Assignment, Error<Rule>> {
        Ok(match_nodes!(input.into_children();
            [symbol(a), term_outer(b), EOI(_)] => Assignment{lhs: a.into_symbol().unwrap(), rhs: b},
        ))
    }
    fn just_symbol(input: Node) -> Result<Term, Error<Rule>> {
        Ok(match_nodes!(input.into_children();
            [symbol(a), EOI(_)] => a,
        ))
    }
    fn old_assignment(input: Node) -> Result<Assignment, Error<Rule>> {
        Ok(match_nodes!(input.into_children();
            [symbol(a), term_outer(b)] => Assignment{lhs: a.into_symbol().unwrap(), rhs: b},
        ))
    }
    fn old_align(input: Node) -> Result<Vec<Assignment>, Error<Rule>> {
        Ok(match_nodes!(input.into_children();
            [old_assignment(a).., EOI(_)] => a.collect(),
        ))
    }
}

#[allow(dead_code)]
pub fn parse_term(input_str: &str) -> Term {
    let input = LatexParser::parse(Rule::term, input_str);
    let input = match input {
        Ok(v) => v,
        Err(e) => panic!("{}", e),
    };
    let input = input.single().unwrap();
    match LatexParser::term(input) {
        Ok(v) => v,
        Err(e) => panic!("{}", e),
    }
}

#[allow(dead_code)]
pub fn parse_equation(input_str: &str) -> Assignment {
    let input = LatexParser::parse(Rule::equation, input_str);
    let input = match input {
        Ok(v) => v,
        Err(e) => panic!("{}", e),
    };
    let input = input.single().unwrap();
    match LatexParser::equation(input) {
        Ok(v) => v,
        Err(e) => panic!("{}", e),
    }
}

#[allow(dead_code)]
pub fn parse_old_align(input_str: &str) -> Vec<Assignment> {
    let input = LatexParser::parse(Rule::old_align, input_str);
    let input = match input {
        Ok(v) => v,
        Err(e) => panic!("{}", e),
    };
    let input = input.single().unwrap();
    match LatexParser::old_align(input) {
        Ok(v) => v,
        Err(e) => panic!("{}", e),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::ast::testhelper::{hashmap, subseval};
    use assert_approx_eq::assert_approx_eq;

    #[test]
    fn check_if_parsable() {
        let a = 12.;
        let b = 23.;
        let c = 34.;

        let hm = hashmap![Symbol::str("a") => a, Symbol::str("b") => b, Symbol::str("c") => c];

        parse_term("a_{+}k");
        parse_term("a_\\text{bc}k");
        parse_term("a_{b\\text{c}}");
        assert_approx_eq!(subseval(parse_term("a+b"), &hm), a + b);
        assert_approx_eq!(subseval(parse_term("c(a+b)"), &hm), c * (a + b));
        assert_approx_eq!(subseval(parse_term("+a-b"), &hm), a - b);
        assert_approx_eq!(subseval(parse_term("-a+b"), &hm), -a + b);
        assert_approx_eq!(subseval(parse_term("a-b"), &hm), a - b);
        assert_approx_eq!(subseval(parse_term("-(a-b)"), &hm), -(a - b));
        assert_approx_eq!(subseval(parse_term("\\sin^2(a)"), &hm), a.sin().powi(2));
        assert_approx_eq!(
            subseval(
                parse_term(
                    r"\begin{cases}
            1 & c \leq 0 \\
            2 & c \geq 0
            \end{cases}"
                ),
                &hm
            ),
            2.
        );
    }
}
