use enum_as_inner::EnumAsInner;

#[derive(Debug, Hash, Eq, PartialEq, Clone)]
pub struct Symbol(pub String);
impl Symbol {
    pub fn str(str: &str) -> Symbol {
        let str = str.replace(&['{', '}', '\\', ' ', '\t', '\n'][..], "");
        Symbol(str)
    }
}

#[derive(Debug, EnumAsInner, Clone)]
pub enum Term {
    Number(f64),
    EulersNumber,
    Symbol(Symbol),
    Sum(Vec<Term>),
    Mul(Vec<Term>),
    Pow(Box<(Term, Term)>),
    Sin(Box<Term>),
    Cos(Box<Term>),
    SectionWise(Vec<(Term, Condition)>),
}

#[derive(Debug)]
pub struct Assignment {
    pub lhs: Symbol,
    pub rhs: Term,
}

#[derive(Debug, Clone)]
pub enum CmpOp {
    Equal,
    NotEqual,
    Lesser,
    LesserEqual,
    Greater,
    GreaterEqual,
}

#[derive(Debug, Clone)]
pub struct Condition {
    pub lhs: Term,
    pub cmp_op: CmpOp,
    pub rhs: Term,
}

#[cfg(test)]
pub mod testhelper {
    use super::*;
    use std::collections::HashMap;

    macro_rules! hashmap {
        ($( $key: expr => $val: expr ),*) => {{
            let mut map = ::std::collections::HashMap::new();
            $( map.insert($key, $val); )*
            map
        }}
    }
    pub(crate) use hashmap;
    pub fn subseval(term: Term, vals: &HashMap<Symbol, f64>) -> f64 {
        match term {
            Term::Number(x) => x,
            Term::EulersNumber => std::f64::consts::E,
            Term::Symbol(s) => vals[&s],
            Term::Sum(v) => v.into_iter().map(|x| subseval(x, vals)).sum(),
            Term::Mul(v) => v
                .into_iter()
                .map(|x| subseval(x, vals))
                .fold(1., |r, val| r * val),
            Term::Pow(v) => subseval(v.0, vals).powf(subseval(v.1, vals)),
            Term::Sin(v) => subseval(*v, vals).sin(),
            Term::Cos(v) => subseval(*v, vals).cos(),
            Term::SectionWise(v) => {
                for (term, cond) in v {
                    let b = match cond.cmp_op {
                        CmpOp::Equal => subseval(cond.lhs, vals) == subseval(cond.rhs, vals),
                        CmpOp::NotEqual => subseval(cond.lhs, vals) != subseval(cond.rhs, vals),
                        CmpOp::Lesser => subseval(cond.lhs, vals) < subseval(cond.rhs, vals),
                        CmpOp::Greater => subseval(cond.lhs, vals) > subseval(cond.rhs, vals),
                        CmpOp::LesserEqual => subseval(cond.lhs, vals) <= subseval(cond.rhs, vals),
                        CmpOp::GreaterEqual => subseval(cond.lhs, vals) >= subseval(cond.rhs, vals),
                    };
                    if b {
                        return subseval(term, vals);
                    }
                }
                panic!("no case is true");
            }
        }
    }
}
