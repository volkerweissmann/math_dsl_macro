#[cfg(test)]
#[macro_use]
extern crate math_dsl_macro;
extern crate regex;
mod ast;
mod codegen;
mod pest_parse;

fn main() {}

#[cfg(test)]
#[allow(non_snake_case)]
mod tests {
    #[test]
    fn use_proc_macro() {
        let theta: f64 = 123.;
        let psi: f64 = 123.;
        let R: f64 = 123.;
        let y: f64 = 123.;

        let m: f64 = 123.;
        let rho: f64 = 123.;
        let sigma_y: f64 = 123.;
        let E_Y: f64 = 123.;
        let v_lf: f64 = 123.;
        let gamma_L: f64 = 123.;
        let c_P: f64 = 123.;
        let c_W: f64 = 123.;
        let rho_f: f64 = 123.;
        let u_t: f64 = 123.;
        let mu: f64 = 123.;
        let gamma_W: f64 = 123.;
        let p: f64 = 123.;
        let U_n1: f64 = 123.;
        let U_t1: f64 = 123.;

        dbg!(tex_expr!(
            r"(2\pi \sin\psi)\sin(\psi+\theta)/R^2 - \frac\pi 3 (1-\cos\psi)^2(2+\cos\psi) + \pi \sin^2\psi y/R"
        ));
        tex_equation!(
            r"F = (2\pi \sin\psi)\sin(\psi+\theta)/R^2 - \frac\pi 3 (1-\cos\psi)^2(2+\cos\psi) + \pi \sin^2\psi y/R"
        );
        dbg!(F);
        from_latex_file!("/home/volker/Sync/ila_parent/bonsint/lib/bons2d.tex");
    }
}
